var CV =
{
    "About": 
        {
            "Name": "Urszula Wrzesniewska",
            "City": "Gdynia",
            "Date": "23.02.2019",
            "Country": "Poland",
        }
    ,
    "Schools": [
        {
            "Name": "XYZ",
            "Startdate": 2018,
            "Enddate": 2019,
        }, {
            "Name": "XYZQWERTY",
            "Startdate": 2019,
            "Enddate": null
        },
    ],
    "Work": [
        {
            "Name": "XYZ",
            "Startdate": 2018,
            "Enddate": 2019,
        }, {
            "Name": "XYZQWERTY",
            "Startdate": 2019,
            "Enddate": null,
        }
    ],
    "Hobbies": [
        
            "Travelling", "swimming", "horseback riding", "shooting", "basketball"

    ]
}
